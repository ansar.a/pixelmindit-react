import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
// Web
import Login from './Web/Pages/Auth/Login';
import Home from './Web/Pages/Home';
import Services from './Web/Pages/Services';
import Solutions from './Web/Pages/Solutions';
import GymManagementSoftware from './Web/Pages/GymManagementSoftware';
import Industries from './Web/Pages/Industries';
import ContactUs from './Web/Pages/ContactUs';
import Company from './Web/Pages/Company';
// CPanel
import Dashboard from './Cpanel/Pages/Dashboard/Dashboard';
// main
import HeaderFooterForm from './Cpanel/Pages/HeaderFooter/HeaderFooter';
import HomeForm from './Cpanel/Pages/Home/Home';
// Services
import MobileDevelopmentForm from './Cpanel/Pages/Services/MobileDevelopment/MobileDevelopment';
import WebDevelopmentForm from './Cpanel/Pages/Services/WebDevelopment/WebDevelopment';
import CustomSoftwareDevelopmentForm from './Cpanel/Pages/Services/CustomSoftwareDevelopment/CustomSoftwareDevelopment';
import DigitalMarketingForm from './Cpanel/Pages/Services/DigitalMarketing/DigitalMarketing';
import ICTConsultancyForm from './Cpanel/Pages/Services/ICTConsultancy/ICTConsultancy';
import IPTelephonyForm from './Cpanel/Pages/Services/IPTelephony/IPTelephony';
import InfrastructureSupportServicesForm from './Cpanel/Pages/Services/InfrastructureSupportServices/InfrastructureSupportServices';
import ECommerceForm from './Cpanel/Pages/Services/ECommerce/ECommerce';
// Solutions
import AllSolutionsForm from './Cpanel/Pages/Solutions/AllSolutions/AllSolutions';
import ERPSolutionsForm from './Cpanel/Pages/Solutions/ERPSolutions/ERPSolutions';
import StaffAndCustomerPortalForm from './Cpanel/Pages/Solutions/StaffAndCustomerPortal/StaffAndCustomerPortal';
import HelpdeskSolutionsForm from './Cpanel/Pages/Solutions/HelpdeskSolutions/HelpdeskSolutions';
// Industries
import HealthcareFitnessForm from './Cpanel/Pages/Industries/HealthcareFitness/HealthcareFitness';

function App() {
  let Logged = true;

  return (
    <>
      <BrowserRouter>
        <Routes>
          {(Logged === false) ?
            <>
              <Route path="/" element={<Home />} />
              <Route path="/services/:id" element={<Services />} />
              <Route path="/solutions/:id" element={<Solutions />} />
              <Route path="/gym-management-software" element={<GymManagementSoftware />} />
              <Route path="/industries/:id" element={<Industries />} />
              <Route path="/contact-us" element={<ContactUs />} />
              <Route path="/company" element={<Company />} />
              <Route path="/login" element={<Login />} />
              <Route path="*" element={<div class="min-vh-100 w-100 d-flex align-items-center justify-content-center"><h1>Page Not Found</h1></div>} />
            </>
            :
            <>
              <Route path="/" element={<Dashboard />} />
              <Route path="/header-footer" element={<HeaderFooterForm />} />
              <Route path="/home" element={<HomeForm />} />
              <Route path="/mobile-development" element={<MobileDevelopmentForm />} />
              <Route path="/web-development" element={<WebDevelopmentForm />} />
              <Route path="/custom-software-development" element={<CustomSoftwareDevelopmentForm />} />
              <Route path="/digital-marketing" element={<DigitalMarketingForm />} />
              <Route path="/ict-consultancy" element={<ICTConsultancyForm />} />
              <Route path="/ip-telephony" element={<IPTelephonyForm />} />
              <Route path="/infrastructure-support-services" element={<InfrastructureSupportServicesForm />} />
              <Route path="/e-commerce" element={<ECommerceForm />} />
              <Route path="/healthcare-fitness" element={<HealthcareFitnessForm />} />
              <Route path="/all-solutions" element={<AllSolutionsForm />} />
              <Route path="/erp-solutions" element={<ERPSolutionsForm />} />
              <Route path="/staff-and-customer-portal" element={<StaffAndCustomerPortalForm />} />
              <Route path="/helpdesk-solutions" element={<HelpdeskSolutionsForm />} />
              <Route path="*" element={<div class="min-vh-100 w-100 d-flex align-items-center justify-content-center"><h1>Page Not Found</h1></div>} />
            </>
          }
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App