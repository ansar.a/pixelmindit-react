import React from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import { Parallax } from 'react-parallax';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

// ServiceOffering
import serv1 from '../../assets/Home/ServiceOffering/mobile-development.png';
import serv2 from '../../assets/Home/ServiceOffering/digital-marketing.png';
import serv3 from '../../assets/Home/ServiceOffering/custom-software-dev.png';
import serv4 from '../../assets/Home/ServiceOffering/web-development.png';
import serv5 from '../../assets/Home/ServiceOffering/ict-consult.png';

// Clients
import c1 from '../../assets/Home/Clients/main-logo-trainfitness.png';
import c2 from '../../assets/Home/Clients/plfc.png';
import c3 from '../../assets/Home/Clients/primeSmallSize.png';
import c4 from '../../assets/Home/Clients/rosefit.png';
import c5 from '../../assets/Home/Clients/trainway.png';
import c6 from '../../assets/Home/Clients/cosmic-logo-scaled.jpg';
import c7 from '../../assets/Home/Clients/district.png';
import c8 from '../../assets/Home/Clients/greens.png';
import c9 from '../../assets/Home/Clients/grip.png';
import c10 from '../../assets/Home/Clients/LoginSaltLogo.jpg';

// Partners
import p1 from '../../assets/Home/Partners/unify-partner.png';
import p2 from '../../assets/Home/Partners/symantec.png';
import p3 from '../../assets/Home/Partners/adobe-partner.png';
import p4 from '../../assets/Home/Partners/micrsoft-parner.png';
import p5 from '../../assets/Home/Partners/dell-partner.png';
import p6 from '../../assets/Home/Partners/kespersy-partner.png';
import p7 from '../../assets/Home/Partners/trend-micro-partner.png';
import p8 from '../../assets/Home/Partners/vmware-partner.png';

// Solution
import s1 from '../../assets/Home/HomeSolutions/Gymnago.png';
import s2 from '../../assets/Home/HomeSolutions/ERP.png';
import s3 from '../../assets/Home/HomeSolutions/qfasto-logo.png';

// ParallaxImage
import plxImg from '../../assets/parallax-bg-latest.jpg';

function Home() {

    const ClientPartnerCarouselResponsive = {
        xxl: {
            breakpoint: { max: 4096, min: 1400 },
            items: 6
        },
        xl: {
            breakpoint: { max: 1400, min: 1200 },
            items: 5
        },
        lg: {
            breakpoint: { max: 1200, min: 992 },
            items: 4
        },
        md: {
            breakpoint: { max: 992, min: 768 },
            items: 3
        },
        sm: {
            breakpoint: { max: 768, min: 576 },
            items: 2
        },
        xs: {
            breakpoint: { max: 576, min: 0 },
            items: 1
        }
    };

    const solutionsCarouselResponsive = {
        xxl: {
            breakpoint: { max: 4096, min: 1400 },
            items: 3
        },
        xl: {
            breakpoint: { max: 1400, min: 1200 },
            items: 3
        },
        lg: {
            breakpoint: { max: 1200, min: 992 },
            items: 2
        },
        md: {
            breakpoint: { max: 992, min: 768 },
            items: 2
        },
        sm: {
            breakpoint: { max: 768, min: 576 },
            items: 1
        },
        xs: {
            breakpoint: { max: 576, min: 0 },
            items: 1
        }
    };

    return (

        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        <div className="col-12">
                            <Header />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="row">
                                {/* section 1 */}
                                <section className="col-12 position-relative">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-12 py-4 text-center position-relative">
                                                <h2 className="my-4">Who are we & What we do</h2>
                                                <p className='my-4'>Pixel Mind IT solutions enables Digital Transformation for enterprises and top agencies around world by delivering seamless customer experience, business efficiency and actionable insights through an integrated set of disruptive technologies. Pixel Mind IT Solutions offers custom software development, Website & Mobile App Development, Digital marketing, customers’ portals and consulting.</p>
                                                <a href="#" className="btn btn-danger br-50px my-4">Know More</a>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                {/* section 2 */}
                                <section className="container-fluid service-fluid">
                                    <h2 className="text-center px-5 my-5" style={{ fontSize: "30px" }}>Service Offerings</h2>
                                    <div className="container service-container d-flex flex-wrap flex-md-no-wrap justify-content-between px-0 pb-5">
                                        <div className="service-items">
                                            <div className="service-items-child">
                                                <img className="img-fluid" src={serv1} />
                                                <h3>Mobile App Development</h3>
                                                <div className="service-items-child-hover">
                                                    <p className="who-we-h-p">Pixel Mind IT Solutions, a trusted software services company, is making mobile application development by uniting extensive experience and ideations of innovation in order to make enterprises truly distinctive. </p>
                                                    <a href="#"><button className="who-we-h-btn">Read More</button></a>
                                                </div>
                                            </div>
                                            <div className="service-items-child">
                                                <img className="img-fluid" src={serv2} />
                                                <h3>Digital Marketing</h3>
                                                <div className="service-items-child-hover">
                                                    <p className="who-we-h-p">Our skilled team of social marketers will create, manage, and deliver top-performing social media campaigns for your business. As a digital marketing leaders...</p>
                                                    <a href="#"><button className="who-we-h-btn">Read More</button></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="service-items">
                                            <div className="service-items-child h-100">
                                                <img className="img-fluid h-100" src={serv3} />
                                                <h3>Custom Software Development</h3>
                                                <div className="service-items-child-hover">
                                                    <p className="who-we-h-p two-item">At Pixel Mind IT, as a leading custom Software Application Development Company, we explicate your views and draft a roadmap accordingly for the software development. At every stage of SDLC i.e. requirement gathering, strategizing, creating a model, implementation, and deployment, we maintain constant communication with you so that nothing goes unnoticed and if any risks are identified it can be dealt immediately. </p>
                                                    <a href="#"><button className="who-we-h-btn">Read More</button></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="service-items">
                                            <div className="service-items-child">
                                                <img className="img-fluid" src={serv4} />
                                                <h3>Website Designing &amp; Development</h3>
                                                <div className="service-items-child-hover">
                                                    <p className="who-we-h-p">At Pixel Mind IT solutions, our skilled team of web developers having the decade’s long expertise are capable of composing high-end web software...</p>
                                                    <a href="#"><button className="who-we-h-btn">Read More</button></a>
                                                </div>
                                            </div>
                                            <div className="service-items-child">
                                                <img className="img-fluid" src={serv5} />
                                                <h3>ICT Consultancy</h3>
                                                <div className="service-items-child-hover">
                                                    <p className="who-we-h-p">Pixel Mind IT solutions is top IT solutions provider with proven track record for past 10 years, onboard experienced consultants who deliver expert insight in accord with the clients’ </p>
                                                    <a href="#"><button className="who-we-h-btn">Read More</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                {/* section 3 */}
                                <section className="container-fluid py-5 we-do-fluid">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-lg-10 col-md-12 col-sm-12 col-12 lead">
                                                <h3 className='text-white' style={{ fontSize: "28px" }}>We enable our customers to invent tomorrow</h3>
                                            </div>
                                            <div className="col-lg-2 col-md-12 col-sm-12 col-12 d-flex justify-center align-items-center">
                                                <a href="#" className="btn btn-light br-50px">Know More</a>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                {/* section 4 */}
                                <section className="container-fluid pt-5">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-12 text-center">
                                                <h2 className="pb-2" style={{ fontSize: "30px" }}>Some of our Clients</h2>
                                                <p>Pixel Mind IT is proud to have happy clients.</p>
                                            </div>
                                            <div className="col-12">
                                                <Carousel
                                                    swipeable={false}
                                                    draggable={false}
                                                    showDots={true}
                                                    autoPlay={true}
                                                    autoPlaySpeed={2000}
                                                    removeArrowOnDeviceType={["xxl", "xl", "lg", "md", "sm", "xs"]}
                                                    infinite={true}
                                                    responsive={ClientPartnerCarouselResponsive}
                                                    className="clientCarousel mt-4 pb-5 mb-3"
                                                >
                                                    <div className='px-2'>
                                                        <img src={c1} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c2} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c3} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c4} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c5} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c6} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c7} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c8} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c9} className="rmc-products-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={c10} className="rmc-products-img" />
                                                    </div>
                                                </Carousel>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                {/* section 5 */}
                                <section className="container-fluid py-5 px-0">
                                    <Parallax bgImage={plxImg} bgImageAlt="the cat" strength={400}>
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-12 text-center">
                                                    <h2 className="py-2 my-4 text-white" style={{ fontSize: "30px" }}>Solutions</h2>
                                                </div>
                                                <div className="col-12">
                                                    <Carousel
                                                        swipeable={false}
                                                        draggable={false}
                                                        showDots={false}
                                                        autoPlay={true}
                                                        autoPlaySpeed={5000}
                                                        removeArrowOnDeviceType={["xxl", "xl", "lg", "md", "sm", "xs"]}
                                                        infinite={true}
                                                        responsive={solutionsCarouselResponsive}
                                                        className="partnersCarousel mt-4 pb-5 mb-3"
                                                    >
                                                        <div className='px-2'>
                                                            <div className="card w-100 bg-transparent-black">
                                                                <div className="card-body">
                                                                    <div className="d-flex justify-content-center w-100">
                                                                        <img style={{ width: "44px" }} className="my-3" src={s1} />
                                                                    </div>
                                                                    <h5 className="w-100 text-center text-white"><b>Gym Management System</b></h5>
                                                                    <p className="mb-5 text-white" style={{ fontSize: "15px" }}>GymnaGo is our solution for gyms and fitness businesses of all sizes to manage all operations that their modern gym needs in one platform with our integrated emerging tools and technologies...</p>
                                                                    <div className="d-flex justify-content-center w-100 py-3">
                                                                        <a href="#" className="text-danger d-inline-flex align-items-center">
                                                                            <span className='mx-1'>View more</span>
                                                                            <FontAwesomeIcon className='mx-1' icon={faArrowRight} />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='px-2'>
                                                            <div className="card w-100 bg-transparent-black">
                                                                <div className="card-body">
                                                                    <div className="d-flex justify-content-center w-100">
                                                                        <img style={{ width: "50px" }} className="my-3" src={s2} />
                                                                    </div>
                                                                    <h5 className="w-100 text-center text-white"><b>ERP Solutions</b></h5>
                                                                    <p className="mb-5 text-white" style={{ fontSize: "15px" }}>Pixel Mind IT Solution global recognized leader in developing and delivering ERP. Our system fully managing all operations on the business transaction and integrated with others to help...</p>
                                                                    <div className="d-flex justify-content-center w-100 py-3">
                                                                        <a href="#" className="text-danger d-inline-flex align-items-center">
                                                                            <span className='mx-1'>View more</span>
                                                                            <FontAwesomeIcon className='mx-1' icon={faArrowRight} />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='px-2'>
                                                            <div className="card w-100 bg-transparent-black">
                                                                <div className="card-body">
                                                                    <div className="d-flex justify-content-center w-100">
                                                                        <img style={{ width: "59px" }} className="my-3" src={s3} />
                                                                    </div>
                                                                    <h5 className="w-100 text-center text-white"><b>Queue Management System</b></h5>
                                                                    <p className="mb-5 text-white" style={{ fontSize: "15px" }}>QFasto is new era on managing the Queue digitally, we use innovative techniques that makes individual and organization benefit from our solution on highly effective ways.</p>
                                                                    <div className="d-flex justify-content-center w-100 py-3">
                                                                        <a href="#" className="text-danger d-inline-flex align-items-center">
                                                                            <span className='mx-1'>View more</span>
                                                                            <FontAwesomeIcon className='mx-1' icon={faArrowRight} />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Carousel>
                                                </div>
                                            </div>
                                        </div>
                                    </Parallax>
                                </section>
                                {/* section 6 */}
                                <section className="container-fluid pb-5">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-12 text-center">
                                                <h2 className="pb-2" style={{ fontSize: "30px" }}>Our Partners</h2>
                                                <p>Pixel Mind are proud to be partners with some of the worlds most dynamic brands pushing the boundaries in technology.</p>
                                            </div>
                                            <div className="col-12">
                                                <Carousel
                                                    swipeable={false}
                                                    draggable={false}
                                                    showDots={true}
                                                    autoPlay={true}
                                                    autoPlaySpeed={2000}
                                                    removeArrowOnDeviceType={["xxl", "xl", "lg", "md", "sm", "xs"]}
                                                    infinite={true}
                                                    responsive={ClientPartnerCarouselResponsive}
                                                    className="partnersCarousel mt-4 pb-5 mb-3"
                                                >
                                                    <div className='px-2'>
                                                        <img src={p1} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p2} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p3} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p4} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p5} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p6} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p7} className="rmc-partners-img" />
                                                    </div>
                                                    <div className='px-2'>
                                                        <img src={p8} className="rmc-partners-img" />
                                                    </div>
                                                </Carousel>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Footer />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home