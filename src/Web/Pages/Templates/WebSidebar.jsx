import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebookF, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'

function WebSidebar() {
    let webSidebarOpen = false;

    function webSidebarChange() {
        webSidebarOpen = !webSidebarOpen;
        console.log(webSidebarOpen);
        const sideBar = document.querySelector('#sideBar');
        const navIcon1 = document.querySelector('#navIcon1');
        if (webSidebarOpen === true) {
            sideBar.classList.add("active");
            navIcon1.classList.add("open");
        } else {
            sideBar.classList.remove("active");
            navIcon1.classList.remove("open");
        }
    }

    return (
        <div className="side-b-wrapper position-absolute">
            {/* left links  */}
            <nav id="sideBar">
                <ul className="list-unstyled components">
                    <li>
                        <a href="#">Company</a>
                    </li>
                    <li>
                        <a href="#">Careers</a>
                    </li>
                    <li>
                        <a href="#">Location</a>
                    </li>
                </ul>
            </nav>
            {/* closer area */}
            <div id="sideContent">
                <nav className="navbar navbar-expand-lg custom-nav d-block">
                    <div className="container-fluid d-block mb-5">
                        <div className="nav-icon1" id="navIcon1" onClick={webSidebarChange}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div className="position-absolute" id="navbarSupportedContent">
                            <ul>
                                <li className="list-unstyled mb-2"><a href="#" target="_blank" className='text-white'><FontAwesomeIcon icon={faTwitter} /></a></li>
                                <li className="list-unstyled mb-2"><a href="#" target="_blank" className='text-white'><FontAwesomeIcon icon={faFacebookF} /></a></li>
                                <li className="list-unstyled mb-2"><a href="#" target="_blank" className='text-white'><FontAwesomeIcon icon={faLinkedinIn} /></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export default WebSidebar