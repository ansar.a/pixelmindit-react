import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLocationDot } from '@fortawesome/free-solid-svg-icons'
import hSlide4 from '../../../assets/Home/HomeSlider/webbanner.jpg';
import hSlide3 from '../../../assets/Home/HomeSlider/mobilebanner.jpg';
import hSlide2 from '../../../assets/Home/HomeSlider/qfastobanner.jpg';
import hSlide1 from '../../../assets/Home/HomeSlider/gymbanner.jpg';
import GI from '../../../assets/Home/HomeSlider/gymnagoimg.png';
import QI from '../../../assets/Home/HomeSlider/qfastoimg.png';

function HomeCarousel() {
    const [HomeSliderHeight, setHomeSliderHeight] = useState(getHomeSliderHeight());

    function getHomeSliderHeight() {
        let mainNavHeight = 90;
        const { innerHeight: height } = window;
        let HomeSliderHeight = height - mainNavHeight;
        return HomeSliderHeight;
    }

    useEffect(() => {
        const myCarousel = document.getElementById('HomeCarousel');
        const HomeCarouselCaptionImage = document.querySelectorAll('.HomeCarouselCaptionImage');
        const HomeCarouselHead = document.querySelectorAll('.HomeCarouselHead');
        const HomeCarouselLink = document.querySelectorAll('.HomeCarouselLink');
        function attachAnimate() {
            if (HomeCarouselCaptionImage.length > 0) {
                HomeCarouselCaptionImage.forEach((single) => {
                    single.classList.remove("animate__zoomIn");
                });
            } else { }
            if (HomeCarouselHead.length > 0) {
                HomeCarouselHead.forEach((single) => {
                    single.classList.remove("animate__zoomIn");
                });
            } else { }
            if (HomeCarouselLink.length > 0) {
                HomeCarouselLink.forEach((single) => {
                    single.classList.remove("animate__fadeInUp");
                });
            } else { }
        }
        myCarousel.addEventListener('slide.bs.carousel', attachAnimate)
        return () => myCarousel.removeEventListener('slide.bs.carousel', attachAnimate);
    });

    useEffect(() => {
        const myCarousel = document.getElementById('HomeCarousel');
        function attachAnimate() {
            const HomeCarouselCaptionImage = document.querySelectorAll('.HomeCarouselCaptionImage');
            const HomeCarouselHead = document.querySelectorAll('.HomeCarouselHead');
            const HomeCarouselLink = document.querySelectorAll('.HomeCarouselLink');

            if (HomeCarouselCaptionImage.length > 0) {
                console.log();
                HomeCarouselCaptionImage.forEach((single) => {
                    single.classList.add("animate__zoomIn");
                });
            } else { }
            if (HomeCarouselHead.length > 0) {
                HomeCarouselHead.forEach((single) => {
                    single.classList.add("animate__zoomIn");
                });
            } else { }
            if (HomeCarouselLink.length > 0) {
                HomeCarouselLink.forEach((single) => {
                    single.classList.add("animate__fadeInUp");
                });
            } else { }
        }
        myCarousel.addEventListener('slid.bs.carousel', attachAnimate)
        return () => myCarousel.removeEventListener('slid.bs.carousel', attachAnimate);
    });


    return (
        <>
            {/*<!-- Carousel -->*/}
            <div id="HomeCarousel" className="carousel slide" data-bs-ride="carousel">
                {/*<!-- Indicators/dots -->*/}
                <div className="carousel-indicators">
                    <button type="button" data-bs-target="#HomeCarousel" data-bs-slide-to="0" className="active"></button>
                    <button type="button" data-bs-target="#HomeCarousel" data-bs-slide-to="1"></button>
                    <button type="button" data-bs-target="#HomeCarousel" data-bs-slide-to="2"></button>
                    <button type="button" data-bs-target="#HomeCarousel" data-bs-slide-to="3"></button>
                </div>
                {/*<!-- The slideshow/carousel -->*/}
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img src={hSlide1} alt="" className="d-block w-100 HomeCarouselImage" style={{ height: HomeSliderHeight }} />
                        <div className="carousel-caption text-start">
                            <img src={GI} alt="" className="HomeCarouselCaptionImage mb-3 w-200px animate__animated" />
                            <h3 className='HomeCarouselHead animate__animated'>Raise The Bar Hight...<br />Let your Gym Stand Out!</h3>
                            <a href='#' target="_blank" className="HomeCarouselLink btn btn-danger mt-2 mb-4 px-3 animate__animated">EXPLORE</a>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img src={hSlide2} alt="" className="d-block w-100 HomeCarouselImage" style={{ height: HomeSliderHeight }} />
                        <div className="carousel-caption text-start">
                            <img src={QI} alt="" className="HomeCarouselCaptionImage mb-3 w-200px animate__animated" />
                            <h3 className='HomeCarouselHead animate__animated'>Quick and Easy queuing with QFasto</h3>
                            <a href='#' target="_blank" className="HomeCarouselLink btn btn-danger mt-2 mb-4 px-3 animate__animated">EXPLORE</a>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img src={hSlide3} alt="" className="d-block w-100 HomeCarouselImage" style={{ height: HomeSliderHeight }} />
                        <div className="carousel-caption text-start">
                            <h3 className='HomeCarouselHead animate__animated'>Think future and build scalable, reliable and next gen mobile apps.</h3>
                            <a href='#' target="_blank" className="HomeCarouselLink btn btn-danger mt-2 mb-4 px-3 animate__animated">EXPLORE</a>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img src={hSlide4} alt="" className="d-block w-100 HomeCarouselImage" style={{ height: HomeSliderHeight }} />
                        <div className="carousel-caption text-start">
                            <h3 className='HomeCarouselHead animate__animated'>Drive innovative growth with digitally transformed web development</h3>
                            <a href='#' target="_blank" className="HomeCarouselLink btn btn-danger mt-2 mb-4 px-3 animate__animated">EXPLORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="position-absolute text-end" style={{ top: "0", left: "0", right: "0" }}>
                <a href='#' className='btn p-0 m-2 text-white'><FontAwesomeIcon icon={faLocationDot} /><span className='ps-2'>Contact Us</span></a>
            </div>
        </>
    )
}

export default HomeCarousel