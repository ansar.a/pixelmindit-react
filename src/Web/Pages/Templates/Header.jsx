import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import WebSidebar from './WebSidebar';
import HomeCarousel from './HomeCarousel';
import pWhiteLogo from '../../../assets/PixelMindIT-logo-white.png';
import pColorLogo from '../../../assets/PixelMindIT-logo-color.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLocationDot } from '@fortawesome/free-solid-svg-icons'

function Header({ bannerTitle, bannerBg }) {
    const [homePage, setHomePage] = useState(checkHomePage());

    function checkHomePage() {
        let obj = useLocation();
        let currentPageUrl = obj.pathname;
        let rtnVal = (currentPageUrl === '/') ? true : false;
        return rtnVal;
    }

    useEffect(() => {
        function handleResize() {
            setHomeSliderHeight(getHomeSliderHeight());
        }
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    });

    useEffect(() => {
        window.addEventListener('scroll', isSticky);
        return () => window.removeEventListener('scroll', isSticky);
    });

    const isSticky = (e) => {
        const mainNavWrapper = document.querySelector('#mainNavWrapper');
        const mainNav = document.querySelector('#mainNav');
        let sticky = mainNavWrapper.offsetTop;
        if (window.pageYOffset >= sticky) {
            mainNav.classList.add("mainNavSticky");
        } else {
            mainNav.classList.remove("mainNavSticky");
        }
    };

    return (
        <div className="row">
            {homePage === true &&
                <div className="col-12 px-0 position-relative">
                    <HomeCarousel />
                    <WebSidebar />
                </div>
            }
            <div className="col-12 px-0" id="mainNavWrapper">
                <nav className={`px-3 px-lg-5 navbar navbar-expand-lg ${homePage ? "mainNavHome bg-black navbar-dark" : "mainNavNotHome bg-white navbar-light"}`} id="mainNav">
                    <div className="container px-3 ps-lg-5">
                        <a className="navbar-brand" href="#">
                            {homePage === true &&
                                <img src={pWhiteLogo} alt="Logo" className="w-100" />
                            }
                            {homePage === false &&
                                <img src={pColorLogo} alt="Logo" className="w-100" />
                            }
                        </a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul className="navbar-nav ms-0 ms-xl-4 ms-xxl-5">
                                <li className="nav-item">
                                    {/* <a className="nav-link" href="#">Home</a> */}
                                    <Link to="/" className="nav-link" href="#">Home</Link>
                                </li>
                                <li className="nav-item dropdown ms-0 ms-xl-3">
                                    <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Services</a>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/services/mobile-app-development" className="dropdown-item">Mobile Development</Link></li>
                                        <li><Link to="/services/web-app-development" className="dropdown-item">Web development</Link></li>
                                        <li><Link to="/services/custom-software-development" className="dropdown-item">Software Development</Link></li>
                                        <li><Link to="/services/digital-marketing" className="dropdown-item">Digital Marketing</Link></li>
                                        <li><Link to="/services/ict-consultancy" className="dropdown-item">ICT consultancy</Link></li>
                                        <li><Link to="/services/ip-telephony" className="dropdown-item">IP Telephony</Link></li>
                                        <li><Link to="/services/infrastructure-support-services" className="dropdown-item">Infrastructure Support Services</Link></li>
                                        <li><Link to="/services/e-commerce" className="dropdown-item">E-Commerce</Link></li>
                                    </ul>
                                </li>
                                <li className="nav-item dropdown ms-0 ms-xl-3">
                                    <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Solutions</a>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/solutions/all-solutions" className="dropdown-item">All Solutions</Link></li>
                                        <li><Link to="#" className="dropdown-item">School Management Software</Link></li>
                                        <li><Link to="/gym-management-software" className="dropdown-item">Gym Management Software</Link></li>
                                        <li><a href="#" className="dropdown-item">Queue Management System</a></li>
                                        <li><Link to="/solutions/erp-solutions" className="dropdown-item">ERP Solutions</Link></li>
                                        <li><Link to="/solutions/staff-and-customer-portal" className="dropdown-item">Staff & Customer Portal</Link></li>
                                        <li><Link to="/solutions/helpdesk-solutions" className="dropdown-item">Helpdesk solutions</Link></li>
                                        <li><Link to="/solutions/e-tendering" className="dropdown-item">E-Tendering</Link></li>
                                        <li><Link to="/solutions/customers-relationship-management" className="dropdown-item">Customers Relationship Management</Link></li>
                                        <li><Link to="/solutions/time-and-attendance-system" className="dropdown-item">Time & Attendance System</Link></li>
                                    </ul>
                                </li>
                                <li className="nav-item dropdown ms-0 ms-xl-3">
                                    <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Industries</a>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/industries/healthcare-fitness" className="dropdown-item">Healthcare Fitness</Link></li>
                                        <li><Link to="/industries/e-learning-and-education" className="dropdown-item">E-learning and Education</Link></li>
                                        <li><Link to="/industries/banking-and-finance" className="dropdown-item">Banking & Finance</Link></li>
                                        <li><Link to="/industries/retail-and-e-commerce" className="dropdown-item">Retail & E-Commerce</Link></li>
                                        <li><Link to="/industries/government" className="dropdown-item">Government</Link></li>
                                        <li><Link to="/industries/telecommunications" className="dropdown-item">Telecommunication</Link></li>
                                    </ul>
                                </li>
                            </ul>
                            {homePage === false &&
                                <ul className="navbar-nav ms-0 ms-xl-auto">
                                    <li className="nav-item">
                                        <a className="nav-link text-danger" href="#"><FontAwesomeIcon icon={faLocationDot} /><span className='ms-2'>Contact Us</span></a>
                                    </li>
                                </ul>
                            }
                        </div>
                    </div>
                </nav>
            </div>
            {homePage === false &&
                <div className="col-12 px-0 position-relative">
                    <div className='bg-secondary d-flex align-items-center justify-content-center position-relative overflow-hidden' style={{ height: "500px" }}>
                        {bannerBg &&
                            <img src={bannerBg} className='w-100 h-100 position-absolute object-fit-cover' style={{ top: 0, left: 0 }} />
                        }
                        {bannerTitle &&
                            <h1 className='text-center text-white mt-4 mb-0 position-relative' style={{ fontWeight: "900", fontSize: "50px" }}>{bannerTitle}</h1>
                        }
                    </div>
                    <WebSidebar />
                </div>
            }
        </div>
    )
}

export default Header