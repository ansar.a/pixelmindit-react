import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebookF, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'

function Footer() {

    return (
        <div className="row">
            <div className="col-12">
                <div className="row">
                    <footer className="col-12 bg-dark text-white border-bottom pt-5 pb-4">
                        <div className="row">
                            <div className="col-12">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-3 footer-menu-col">
                                            <h5>Services</h5>
                                            <ul className='px-0'>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Mobile Development</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Web development</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Software Development</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Digital Marketing</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">ICT consultancy</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">IP Telephony</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Infrastructure Support Services</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">E-Commerce</a></li>
                                            </ul>
                                        </div>
                                        <div className="col-md-3 footer-menu-col">
                                            <h5>Solutions</h5>
                                            <ul className='px-0'>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">ERP Solutions</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Staff & Customer Portal</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Helpdesk solutions</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">E-Tendering</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Customers Relationship Management</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Time & Attendance System</a></li>
                                            </ul>
                                        </div>
                                        <div className="col-md-3 footer-menu-col">
                                            <h5>About Us</h5>
                                            <ul className='px-0'>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Company</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Our Team</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Career</a></li>
                                                <li className='list-style-none my-1'><a href="#" className="text-white">Location</a></li>
                                            </ul>
                                        </div>
                                        <div className="col-md-3 footer-menu-col">
                                            <h5>Connect at</h5>
                                            <ul className="px-0 f-social">
                                                <li className='list-style-none d-inline-block my-1 me-3'><a href="#" className="text-white"><FontAwesomeIcon icon={faTwitter} /></a></li>
                                                <li className='list-style-none d-inline-block my-1 me-3'><a href="#" className="text-white"><FontAwesomeIcon icon={faFacebookF} /></a></li>
                                                <li className='list-style-none d-inline-block my-1 me-3'><a href="#" className="text-white"><FontAwesomeIcon icon={faLinkedinIn} /></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <div className="col-12 bg-dark text-white py-3">
                        <div className="container">
                            <p className='text-white mb-0'>2022 © Copyright Pixel Mind IT. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer