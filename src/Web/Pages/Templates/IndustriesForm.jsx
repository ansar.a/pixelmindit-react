import React from 'react';

function IndustriesForm({ FormCalledFor }) {
    return (
        <>
            <div className='row px-3 py-5'>
                <div className="col-12 py-0 py-lg-4">
                    <div className="s-industries-shadow-form shadow">
                        <h4 className="text-white py-4 text-center" style={{ fontWeight: "700", backgroundColor: "#202c5d" }}>Request a Consultation</h4>
                        <form className="px-3 px-lg-5 px-xl-5">
                            <div className="s-industries-form-items">
                                <div className="form-group ">
                                    <input type="text" name="name-av" value="" size="40" className="form-control" id="name" aria-required="true" aria-invalid="false" placeholder="Full Name" />
                                </div>
                                <div className="form-group">
                                    <input type="email" name="email" value="" size="40" className="form-control" id="email" aria-required="true" aria-invalid="false" placeholder="Email Address" />
                                </div>
                                <div className="form-group">
                                    <input type="tel" name="tel" value="" size="40" maxlength="13" minlength="10" className="form-control" id="tel" aria-required="true" aria-invalid="false" placeholder="Mobile No" />
                                </div>
                                <div className="form-group">
                                    <input type="text" name="company" value="" size="40" className="form-control" id="company" aria-required="true" aria-invalid="false" placeholder="Company" />
                                </div>
                                <div className="form-group">
                                    <input type="text" name="message" value="" size="40" className="form-control" id="message" aria-required="true" aria-invalid="false" placeholder="Message" />
                                </div>
                            </div>
                            <div className="py-4 text-center">
                                <input type="submit" value="Submit" className="btn btn-danger rounded-pill px-4" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default IndustriesForm