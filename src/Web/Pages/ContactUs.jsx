import React, { useState } from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import ContactUsData from './../Data/ContactUs/ContactUsData.json';

function ContactUs() {
    const [data, setData] = useState(initData());

    function initData() {
        return ContactUsData;
    }

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={data.title} bannerBg={data.bannerImagePath} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div id="innerpage-main-body">
                                    <div className="container">
                                        <div className="container contactTwo pb-5">
                                            <div className="row shadow mt-3">
                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-5 col-xxl-5 bg-danger p-5 text-white">
                                                    <h2 className="text-center"><b>Contact Information</b></h2>
                                                    <div className="underLineSolid text-center my-3"></div>
                                                    <div className="ContactNumbers d-flex flex-wrap pt-4">
                                                        <span className="mx-2 h2 icon-phone"></span>
                                                        <div>
                                                            <p className="text-white mt-1">
                                                                <span className="mx-1 dirltrtar d-inline-block">+971 50 320 4999</span>
                                                                <span className="mx-2">|</span>
                                                                <span className="mx-1 dirltrtar d-inline-block">+973 39 611 888</span>
                                                            </p>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                    <div className="ContactEmail d-flex flex-wrap pt-3">
                                                        <span className="mx-2 h2 icon-email"></span>
                                                        <span className="mx-2">sales@pixelmindit.com</span>
                                                    </div>
                                                    <div className="ContactSocial d-flex flex-wrap justify-content-center pt-5">
                                                        <a target="_blank" href="" className="text-white pt-3"><span className="mx-2 h1 icon-facebook"></span></a>
                                                        <a target="_blank" href="" className="text-white pt-3"><span className="mx-2 h1 icon-twitter"></span></a>
                                                        <a target="_blank" href="https://www.linkedin.com/company/pixel-mind-it-solutions/" className="text-white pt-3"><span className="mx-2 h1 icon-linkdin"></span></a>
                                                    </div>
                                                </div>
                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-7 col-xxl-7 bg-white">
                                                    <div>
                                                        <form>

                                                            <div className="row pt-5 px-3">
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="FullName" className="mx-sm-2">Full Name</label><br />
                                                                        <span className="wpcf7-form-control-wrap Full-Name"><input type="text" name="Full-Name" size="40" className="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="FullName" /></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="EmailAddress" className="mx-sm-2">Email Address</label><br />
                                                                        <span className="wpcf7-form-control-wrap EmailID"><input type="email" name="EmailID" size="40" className="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="EmailAddress" /></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="MobileNumber" className="mx-sm-2">Mobile Number</label><br />
                                                                        <span className="wpcf7-form-control-wrap MobileNumber"><input type="tel" name="MobileNumber" size="40" className="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control" id="MobileNumber" /></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="GymName" className="mx-sm-2">Company Name</label><br />
                                                                        <span className="wpcf7-form-control-wrap GymName"><input type="text" name="GymName" size="40" className="form-control" id="GymName" /></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="Message" className="mx-sm-2">Message</label><br />
                                                                        <span className="wpcf7-form-control-wrap Message"><textarea name="Message" cols="40" rows="10" className="form-control" id="MessageContactusForm" ></textarea></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                    <div className="form-group">
                                                                        <label htmlFor="SelectCountry" className="mx-sm-2">Select Country</label><br />
                                                                        <span className="SelectCountry"><select name="SelectCountry" className="form-control" id="SelectCountry" ><option value="Afghanistan">Afghanistan</option><option value="Åland Islands">Åland Islands</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="American Samoa">American Samoa</option><option value="AndorrA">AndorrA</option><option value="Angola">Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia">Bolivia</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cape Verde">Cape Verde</option><option value="Cayman Islands">Cayman Islands</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="CongoThe Democratic Republic of the">CongoThe Democratic Republic of the</option><option value="Cook Islands">Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Cote D'Ivoire">Cote D'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guernsey">Guernsey</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option><option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="IranIslamic Republic Of">IranIslamic Republic Of</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Isle of Man">Isle of Man</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="KoreaDemocratic People'S Republic of">KoreaDemocratic People'S Republic of</option><option value="KoreaRepublic of">KoreaRepublic of</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People'S Democratic Republic">Lao People'S Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="MacedoniaThe Former Yugoslav Republic of">MacedoniaThe Former Yugoslav Republic of</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Marshall Islands">Marshall Islands</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="MicronesiaFederated States of">MicronesiaFederated States of</option><option value="MoldovaRepublic of">MoldovaRepublic of</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="Netherlands Antilles">Netherlands Antilles</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Palestinian TerritoryOccupied">Palestinian TerritoryOccupied</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Reunion">Reunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="RWANDA">RWANDA</option><option value="Saint Helena">Saint Helena</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia and Montenegro">Serbia and Montenegro</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan">Sudan</option><option value="Suriname">Suriname</option><option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="TaiwanProvince of China">TaiwanProvince of China</option><option value="Tajikistan">Tajikistan</option><option value="TanzaniaUnited Republic of">TanzaniaUnited Republic of</option><option value="Thailand">Thailand</option><option value="Timor-Leste">Timor-Leste</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos Islands">Turks and Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela">Venezuela</option><option value="Viet Nam">Viet Nam</option><option value="Virgin IslandsBritish">Virgin IslandsBritish</option><option value="Virgin IslandsU.S.">Virgin IslandsU.S.</option><option value="Wallis and Futuna">Wallis and Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select></span><br />
                                                                        <span className="icon-arrow-right SelectDropDown"></span>
                                                                    </div>
                                                                </div>
                                                                <div className="col-12 pb-5">
                                                                    <div className="justify-content-sm-end d-flex">
                                                                        <input type="submit" value="Submit" className="btn btn-primary mx-1 px-4" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {data.Locations &&
                                            <div className="container my-5 p-0">
                                                <h1 className="mb-3 text-center fw-bold">Our Locations</h1>
                                                <div className="d-flex flex-wrap justify-content-center">
                                                    {data.Locations.map((Location, i) => (
                                                        <div className="card m-2 m-xl-4 CntCard">
                                                            <img className="card-img-top" src={Location.Image} alt="" />
                                                            <div className="card-body">
                                                                <h4 className="card-title">
                                                                    <img className="rounded-circle" src={Location.Flag} alt="" width="40" />
                                                                    <span className="fw-bold">{Location.Name}</span>
                                                                </h4>
                                                                <div className="d-flex py-3">
                                                                    <span className="mx-1 h2 icon-map"></span>
                                                                    <small>{Location.Address}</small>
                                                                </div>
                                                                <div className="d-flex flex-wrap py-3">
                                                                    <span className="mx-1 h4 icon-phone"></span>
                                                                    <div>

                                                                        {Location.Phones &&
                                                                            <p className="mb-0">
                                                                                {Location.Phones.map((Phone, j) => (
                                                                                    <span key={j}>
                                                                                        {j >= 1 &&
                                                                                            <small className="mx-2">|</small>
                                                                                        }
                                                                                        <small className="mx-1 dirltrtar d-inline-block" style={{ fontSize: "13px" }}>{Phone}</small>
                                                                                    </span>
                                                                                ))}
                                                                            </p>
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="d-flex py-3">
                                                                    <span className="mx-1 h2 icon-email"></span>
                                                                    <small className="mt-1">{Location.Email}</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ))}
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContactUs