import React, { useState, useEffect } from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import ReachUs from '../../assets/Solutions/ReachUs.png';
import { useParams } from 'react-router-dom';
import DefaultSolutions from './../Data/Solutions/DefaultSolutions.json';
import AllSolutions from './../Data/Solutions/AllSolutions.json';
import ERPSolutions from './../Data/Solutions/ERPSolutions.json';
import StaffAndCustomerPortal from './../Data/Solutions/StaffAndCustomerPortal.json';
import HelpdeskSolutions from './../Data/Solutions/HelpdeskSolutions.json';
import ETendering from './../Data/Solutions/ETendering.json';
import CustomersRelationshipManagement from './../Data/Solutions/CustomersRelationshipManagement.json';
import TimeAndAttendanceSystem from './../Data/Solutions/TimeAndAttendanceSystem.json';

function Solutions() {
    const [data, setData] = useState(initData());
    const params = useParams();

    function initData() {
        return DefaultSolutions;
    }

    useEffect(() => {
        function FetchData() {
            switch (true) {
                case (params.id === "all-solutions"):
                    return AllSolutions;
                    break;
                case (params.id === "erp-solutions"):
                    return ERPSolutions;
                    break;
                case (params.id === "staff-and-customer-portal"):
                    return StaffAndCustomerPortal;
                    break;
                case (params.id === "helpdesk-solutions"):
                    return HelpdeskSolutions;
                    break;
                case (params.id === "e-tendering"):
                    return ETendering;
                    break;
                case (params.id === "customers-relationship-management"):
                    return CustomersRelationshipManagement;
                    break;
                case (params.id === "time-and-attendance-system"):
                    return TimeAndAttendanceSystem;
                    break;
                default:
                    // link to 404 page
                    return DefaultSolutions;
            }
        }
        setData(FetchData())
    })

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={data.title} bannerBg={data.bannerImagePath} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="container">
                                            <div className="row justify-content-center">
                                                <div className="col-12 pt-5">
                                                    <h2 style={{ color: `${data.color}` }}>{data.title}</h2>
                                                    {data.mainDescriptions &&
                                                        data.mainDescriptions.map((mainDescription, i) => (
                                                            <p key={i}>{mainDescription}</p>
                                                        ))
                                                    }
                                                </div>
                                                {data.subDescriptions &&
                                                    <div className='col-12 col-md-6 col-xl-7'>
                                                        {
                                                            data.subDescriptions.map((subDescription, i) => (
                                                                <p key={i}>{subDescription}</p>
                                                            ))
                                                        }
                                                    </div>
                                                }
                                                {data.pageImagePath &&
                                                    <div className='col-12 col-md-6 col-xl-5'>
                                                        <img alt='' className='w-100 my-4' style={{ maxWidth: "390px" }} src={data.pageImagePath} />
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row py-2 mt-4" style={{ backgroundImage: `url(${ReachUs})` }}>
                                    <div className="col-12 py-5 mb-2 text-center">
                                        <h2 className='fw-normal text-white mb-4'>We love to work with you... Are you ready?</h2>
                                        <a href="#" className='btn btn-light border-0 bg-white w-150px br-50px text-black'>Reach Us</a>
                                    </div>
                                </div>
                                {data.advantages &&
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-12 text-center">
                                                        <h2 className='my-5 fw-bold'>Advantages</h2>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="row justify-content-center">
                                                                    {
                                                                        data.advantages.map((advantage, i) => (
                                                                            <div className='col-12 col-sm-6 col-md-4 col-xl-3 d-flex mb-4' key={i}>
                                                                                <div className='bg-light h-100 w-100 px-3'>
                                                                                    <img alt='' className='w-50px h-50px my-3' src={advantage.path} />
                                                                                    <p className='mb-2 fw-bold'>{advantage.title}</p>
                                                                                    <p className='mb-4 small'>{advantage.description}</p>
                                                                                </div>
                                                                            </div>
                                                                        ))
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Solutions