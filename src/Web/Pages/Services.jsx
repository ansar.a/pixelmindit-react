import React, { useState } from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import { useParams } from 'react-router-dom';
import DefaultServices from './../Data/Services/DefaultServices.json';
import MobileAppDevelopment from './../Data/Services/MobileAppDevelopment.json';
import WebAppDevelopment from './../Data/Services/WebAppDevelopment.json';
import CustomSoftwareDevelopment from './../Data/Services/CustomSoftwareDevelopment.json';
import DigitalMarketing from './../Data/Services/DigitalMarketing.json';
import ICTCunsultancy from './../Data/Services/ICTCunsultancy.json';
import IPTelephony from './../Data/Services/IPTelephony.json';
import InfrastructureSupportServices from './../Data/Services/InfrastructureSupportServices.json';
import ECommerce from './../Data/Services/ECommerce.json';
import { useEffect } from 'react';

function Services() {
    const [data, setData] = useState(initData());
    const params = useParams();

    function initData() {
        return DefaultServices;
    }

    useEffect(() => {
        function FetchData() {
            switch (true) {
                case (params.id === "mobile-app-development"):
                    return MobileAppDevelopment;
                    break;
                case (params.id === "web-app-development"):
                    return WebAppDevelopment;
                    break;
                case (params.id === "custom-software-development"):
                    return CustomSoftwareDevelopment;
                    break;
                case (params.id === "digital-marketing"):
                    return DigitalMarketing;
                    break;
                case (params.id === "ict-consultancy"):
                    return ICTCunsultancy;
                    break;
                case (params.id === "ip-telephony"):
                    return IPTelephony;
                    break;
                case (params.id === "infrastructure-support-services"):
                    return InfrastructureSupportServices;
                    break;
                case (params.id === "e-commerce"):
                    return ECommerce;
                    break;
                default:
                    // link to 404 page
                    return DefaultServices;
            }
        }
        setData(FetchData())
    })

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={data.title} bannerBg={data.bannerImagePath} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-12 pt-5">
                                                    <h2 style={{ color: `${data.color}` }}>{data.title}</h2>
                                                    {data.descriptions &&
                                                        data.descriptions.map((description, i) => (
                                                            <p key={i}>{description}</p>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {data.differentiators &&
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="container-fluid">
                                                <div className="row">
                                                    <div className="col-12 text-center">
                                                        <h2 className='my-5 fw-bold'>Our Differentiators</h2>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="row justify-content-center">
                                                                    {
                                                                        data.differentiators.map((differentiator, i) => (
                                                                            <div className='col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 col-xxl-2 mx-0 mx-xl-5' key={i}>
                                                                                <img alt='' className='w-75px h-75px mb-3' src={differentiator.path} />
                                                                                <p className='mb-4'>{differentiator.description}</p>
                                                                            </div>
                                                                        ))
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                <div className="row py-2 mt-4" style={{ backgroundColor: `${data.color}` }}>
                                    <div className="col-12 py-5 mb-2 text-center">
                                        <h2 className='fw-normal text-white mb-4' >HAVE ANY BUSINESS QUERIES?</h2>
                                        <a href="#" className='btn btn-light border-0 bg-white w-150px br-50px text-black'>Reach Us</a>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="container ins-insights-c">
                                            <div className="row">
                                                <div className="col-12 py-2">
                                                    <h2 className='text-center mt-5 mb-5'><span className='d-inline-flex border-bottom border-danger p-2'>Insights</span></h2>
                                                </div>
                                                <div className="col-md-4 pb-5">
                                                    <img className="w-100" style={{ borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} src="/uploads/Insight/casestudy-img.png" />
                                                    <div className="bg-light px-3 py-1">
                                                        <p className='text-danger fw-normal mt-2 mb-1'>Case Study</p>
                                                        <p>To become top leaders in delivering customer centric solutions through...</p>
                                                        <div className="text-end pb-4">
                                                            <a href="#" className='text-danger fw-bold'>Read more</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 pb-5">
                                                    <img className="w-100" style={{ borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} src="/uploads/Insight/brocure-img.png" />
                                                    <div className="bg-light px-3 py-1">
                                                        <p className='text-danger fw-normal mt-2 mb-1'>Brochure</p>
                                                        <p>Here you can have a look on company profile of Pixel mind it solutions ...</p>
                                                        <div className="text-end pb-4">
                                                            <a target="_blank" href="/downloads/pxelmind-profile.pdf" className='text-danger fw-bold'>Download</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 pb-5">
                                                    <img className="w-100" style={{ borderTopLeftRadius: "20px", borderTopRightRadius: "20px" }} src="/uploads/Insight/sample-whitepaper-img.png" />
                                                    <div className="bg-light px-3 py-1">
                                                        <p className='text-danger fw-normal mt-2 mb-1'>Whitepaper</p>
                                                        <p>Our mission is a constant reminder of the fact that we are in the business of...</p>
                                                        <div className="text-end pb-4">
                                                            <a href="#" className='text-danger fw-bold'>Read more</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Services