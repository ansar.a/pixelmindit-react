import React from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import ReachUs from '../../assets/Solutions/ReachUs.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function Company() {
    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={"About Pixel Mind"} bannerBg={"uploads/Company/company-banner.jpg"} />
                            </div>
                        </div>
                        <div className="row">
                            <div id="col-12">

                                <div className="row">
                                    <div className="col-12 bg-light">
                                        <div className="container ab-pixel-head-container">
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ab-pixel-head-col">
                                                    <h2 className='text-center mt-5 mb-3'>Company Overview</h2>
                                                    <div>
                                                        <p>Pixel Mind IT solutions enables Digital Transformation for enterprises and top agencies around world by delivering seamless customer experience, business efficiency and actionable insights through an integrated set of disruptive technologies. Pixel Mind IT Solutions offers custom software development, Website &amp; Mobile App Development, Digital marketing, customers portals and consulting. With combined experience of 18 years in delivering best of class services we have applicability across industry sectors such as retail, e-commerce, banking, insurance, hi-tech, engineering R&amp;D, manufacturing, automotive and travel/transportation/hospitality. <br />
                                                            <br />
                                                            As a true customer centric IT Company, the focus is on “Satisfying our customers comes first” which involves perceiving attentively, processing unbiasedly and performing empathetically. We believe future belongs to those who create it and deliver future ready services to our clients by being the evangelist for latest technologies and tools. We are always looking ahead to anticipate what's next. <br />
                                                            <br />
                                                            AT Pixel Mind, we adapt an innovation-led approach to help our clients develop and deliver disruptive innovations, and to scale them faster. From schools, enterprises and studios, we help customers imagine the future and bring it to life.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-12 pb-5">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h2 className='mt-5 mb-3 text-center'>Our Values</h2>
                                                </div>
                                                <div className="col-12 col-sm-6 py-1 px-4" style={{ backgroundColor: "#eba222" }}>
                                                    <h3 className='mt-5 mb-3 text-white text-center'>Vision</h3>
                                                    {/* <FontAwesomeIcon className='mx-1' icon={faLightbulbOn} /> */}
                                                    <p className='text-white mb-4'>To become top leaders in delivering customer centric solutions through innovative ideas, emerging technologies, top class team and creative thinking. </p>
                                                </div>
                                                <div className="col-12 col-sm-6 py-1 px-4" style={{ backgroundColor: "#7b4291" }}>
                                                    <h3 className='mt-5 mb-3 text-white text-center'>Mission</h3>
                                                    {/* <FontAwesomeIcon className='mx-1' icon={faBullseyePointer} /> */}
                                                    <p className='text-white mb-4'>Our mission is a constant reminder of the fact that we are in the business of making our customers the first priority through world class solutions. We dedicate our business process and technology innovation know-how, deep industry expertise and worldwide resources to working together with clients to make their businesses stronger and inventing the future. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row py-2 mt-4" style={{ backgroundImage: `url(${ReachUs})` }}>
                                    <div className="col-12 py-5 mb-2 text-center">
                                        <h2 className='fw-normal text-white mb-4'>We love to work with you... Are you ready?</h2>
                                        <a href="#" className='btn btn-light border-0 bg-white w-150px br-50px text-black'>Reach Us</a>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-12" style={{ backgroundColor: "#1e65a3" }}>
                                        <div className="container">
                                            <div className="row py-5">
                                                <div className="col-12">
                                                    <h2 className='text-white'>Why Choose Us?</h2>
                                                </div>
                                                <div className="col-lg-8 col-md-6 col-sm-12 col-12">
                                                    <p className='text-white'>Our strongest advantage is our excellent pool of skilled resources, recruited from the finest clan of professionals in the industry. They provide a vital blend of strategic IT consultancy services and technology skills which combined with our unique business model including an innovative offshore development approach, rigorous project &amp; program management methodologies, state-of-the-art communications &amp; computing infrastructure.</p>
                                                    <p className='text-white'>Headquartered in Dubai, Pixel Mind IT has operations across the globe which in sync with our aim of border less geography.</p>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <img className="d-block img-fluid" src="https://pixelmindit.com/wp-content/uploads/2022/05/why-choose-us-2-scaled.jpg" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Company