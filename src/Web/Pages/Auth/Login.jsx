import React from 'react'
import { Link } from 'react-router-dom';
import Logo from '../../../assets/PixelMindIT-logo-color.png'

function Login() {

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12 col-lg-6 mx-auto px-3 px-lg-0 mt-5 mb-4">
                    <Link to="/" className="btn btn-success" href="#">Home</Link>
                    </div>
                    <div className="col-12"></div>
                    <div className="col-12 col-lg-6 mx-auto px-5 py-5 bg-light border rounded">
                        <form>
                            <div className='d-flex justify-content-center py-3'>
                                <img src={Logo} alt="Logo" width="200" />
                            </div>
                            <div className="mb-3 mt-3">
                                <label htmlFor="email" className="form-label">Email:</label>
                                <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="pwd" className="form-label">Password:</label>
                                <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pswd" />
                            </div>
                            <div className="form-check mb-3">
                                <label className="form-check-label">
                                    <input className="form-check-input" type="checkbox" name="remember" /> Remember me
                                </label>
                            </div>
                            <button type="button" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login