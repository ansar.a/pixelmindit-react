import React, { useState, useEffect } from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import { useParams } from 'react-router-dom';
import DefaultIndustries from './../Data/Industries/DefaultIndustries.json';
import HealthcareFitness from './../Data/Industries/HealthcareFitness.json';
import ELearningAndEducation from './../Data/Industries/ELearningAndEducation.json';
import BankingAndFinance from './../Data/Industries/BankingAndFinance.json';
import RetailAndECommerce from './../Data/Industries/RetailAndECommerce.json';
import Government from './../Data/Industries/Government.json';
import TeleCommunications from './../Data/Industries/TeleCommunications.json';
import IndustriesForm from './Templates/IndustriesForm'

function Industries() {
    const [data, setData] = useState(initData());
    const params = useParams();

    function initData() {
        return DefaultIndustries;
    }

    useEffect(() => {
        function FetchData() {
            switch (true) {
                case (params.id === "healthcare-fitness"):
                    return HealthcareFitness;
                    break;
                case (params.id === "e-learning-and-education"):
                    return ELearningAndEducation;
                    break;
                case (params.id === "banking-and-finance"):
                    return BankingAndFinance;
                    break;
                case (params.id === "retail-and-e-commerce"):
                    return RetailAndECommerce;
                    break;
                case (params.id === "government"):
                    return Government;
                    break;
                case (params.id === "telecommunications"):
                    return TeleCommunications;
                    break;
                default:
                    // link to 404 page
                    return DefaultIndustries;
            }
        }
        setData(FetchData())
    })

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={data.title} bannerBg={data.bannerImagePath} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 pt-5 px-4 pb-5">
                                            <h2 style={{ color: `${data.color}` }}>{data.subTitle}</h2>
                                            {data.mainDescriptions &&
                                                data.mainDescriptions.map((mainDescription, i) => (
                                                    <p key={i}>{mainDescription}</p>
                                                ))
                                            }
                                        </div>
                                        <div className="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                            <IndustriesForm FormCalledFor={data.title} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Industries