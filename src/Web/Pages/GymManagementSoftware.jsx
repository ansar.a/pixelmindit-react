import React, { useState, useEffect } from 'react';
import '../../../public/css/Web.css'
import Header from './Templates/Header';
import Footer from './Templates/Footer';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import tickImg from '../../assets/Solutions/tick.png';

import GymManagement from './../Data/GymManagement/GymManagement.json';

function GymManagementSoftware() {
    const [data, setData] = useState(initData());

    function initData() {
        return GymManagement;
    }

    useEffect(() => {
        function FetchData() {
            return GymManagement;
        }
        setData(FetchData())
    })

    const featuresCarouselResponsive = {
        xxl: {
            breakpoint: { max: 4096, min: 1400 },
            items: 3
        },
        xl: {
            breakpoint: { max: 1400, min: 1200 },
            items: 3
        },
        lg: {
            breakpoint: { max: 1200, min: 992 },
            items: 2
        },
        md: {
            breakpoint: { max: 992, min: 768 },
            items: 2
        },
        sm: {
            breakpoint: { max: 768, min: 576 },
            items: 1
        },
        xs: {
            breakpoint: { max: 576, min: 0 },
            items: 1
        }
    };

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <Header bannerTitle={data.title} bannerBg={data.bannerImagePath} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="container">
                                            <div className="row justify-content-center">
                                                <div className='col-12 col-md-6'>
                                                    <img alt='' className='w-100 my-4 d-block mx-auto' style={{ maxWidth: "500px" }} src={data.mainImagePath} />
                                                </div>
                                                <div className='col-12 col-md-6 d-flex align-items-center'>
                                                    <div className='wrapper w-100'>
                                                        {data.mainTitle &&
                                                            <h5 className='mt-5 fw-bold'>{data.mainTitle}</h5>
                                                        }
                                                        {data.mainDescription &&
                                                            <p className='mb-5'>{data.mainDescription}</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {data.features &&
                                    <div className="row mt-4">
                                        <div className="col-12 py-1 bg-black">
                                            <h2 className='my-5 fw-bold text-center text-white'>Features</h2>
                                            <div className="container">
                                                <div className="row">
                                                    <div className='col-12'>
                                                        <Carousel
                                                            swipeable={true}
                                                            draggable={true}
                                                            showDots={false}
                                                            autoPlay={true}
                                                            autoPlaySpeed={5000}
                                                            removeArrowOnDeviceType={["xxl", "xl", "lg", "md", "sm", "xs"]}
                                                            infinite={true}
                                                            responsive={featuresCarouselResponsive}
                                                            className="mt-4 pb-5 mb-3"
                                                        >
                                                            {
                                                                data.features.map((feature, i) => (
                                                                    <div className='px-2' key={i}>
                                                                        <div className="card w-100 bg-white text-center" style={{ minHeight: "400px" }}>
                                                                            <div className="card-body">
                                                                                <div className="d-flex justify-content-center w-100">
                                                                                    <img style={{ width: "44px" }} className="my-3" src={feature.path} />
                                                                                </div>
                                                                                <h5 className="w-100"><b>{feature.title}</b></h5>
                                                                                <p className="mb-5">{feature.description}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                ))
                                                            }
                                                        </Carousel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {data.modules &&
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-12">
                                                        <h4 className='mt-5 fw-bold text-primary text-center'>Modules</h4>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <h1 className='text-center mb-4'><b>{data.moduleTitle}</b></h1>
                                                            </div>
                                                            <div className="col-12">
                                                                <ul id="ModulesTab" className="nav nav-pills justify-content-center" role="tablist">
                                                                    {
                                                                        data.modules.map((module, i) => (
                                                                            <li className="nav-item red-pill mx-2 shadow w-200px" role="presentation" key={i}>
                                                                                <button
                                                                                    type='button'
                                                                                    className={`nav-link text-center p-3 h-100 w-100 d-flex align-items-center ${i === 0 ? "active" : ""}`}
                                                                                    data-bs-toggle="pill"
                                                                                    role="tab"
                                                                                    data-bs-target={`#LsSTbsCount-${i}`}
                                                                                    aria-controls={`LsSTbsCount-${i}`}
                                                                                    aria-selected="true"
                                                                                >
                                                                                    <h5>{module.title}</h5>
                                                                                </button>
                                                                            </li>
                                                                        ))
                                                                    }
                                                                </ul>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="tab-content" id='ModulesTabContent'>
                                                                    {
                                                                        data.modules.map((module, i) => (
                                                                            <div
                                                                                className={`container tab-pane fade ${i === 0 ? "show active" : ""}`}
                                                                                role="tabpanel"
                                                                                id={`LsSTbsCount-${i}`}
                                                                                key={i}
                                                                            >
                                                                                <div className="row">
                                                                                    <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 p-4">
                                                                                        <h2><b>{module.title}</b></h2>
                                                                                        <p>{module.description}</p>
                                                                                        {module.points &&
                                                                                            < ul className="nav">
                                                                                                {
                                                                                                    module.points.map((point, j) => (
                                                                                                        <li className="d-flex mb-2 col-12 col-lg-6 align-items-start" key={j}>
                                                                                                            <img src={tickImg} className="mx-2" style={{ marginTop: "2px", width: "15px" }} />
                                                                                                            <h6 className="mx-2">{point}</h6>
                                                                                                        </li>
                                                                                                    ))
                                                                                                }
                                                                                            </ul>
                                                                                        }
                                                                                    </div>
                                                                                    <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 px-1 text-center">
                                                                                        <img className="img-fluid w-500px mx-auto my-5" src={module.moduleImagePath} />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        ))
                                                                    }
                                                                    <div className="container tab-pane fade" role="tabpanel" id="LsSTbsCount-2">s
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default GymManagementSoftware