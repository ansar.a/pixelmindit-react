import React from 'react';
import { Outlet } from "react-router-dom";
import './WebLayout.css'
import Header from '../Pages/Templates/Header';
import Footer from '../Pages/Templates/Footer';

function WebLayout() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        <div className="col-12">
                            <Header />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Outlet />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Footer />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WebLayout