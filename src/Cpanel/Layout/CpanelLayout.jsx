import React from "react";
import { Outlet } from "react-router-dom";
import TopBar from '../Pages/Templates/TopBar'
import LeftBar from '../Pages/Templates/LeftBar'

function CpanelLayout() {
    return (
        <div className="container-fluid">
            <div className="row">
                <TopBar />
                <LeftBar />
                <Outlet />
            </div>
        </div>
    )
}

export default CpanelLayout