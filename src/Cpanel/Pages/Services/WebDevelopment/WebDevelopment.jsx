import React from 'react'
import TopBar from '../../Templates/TopBar'
import LeftBar from '../../Templates/LeftBar'
import CLogo from '../../../../assets/PixelMindIT-logo-color.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faClose } from '@fortawesome/free-solid-svg-icons'

function WebDevelopmentForm() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <TopBar />
                </div>
                <div className="col-12 col-sm-3 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
                    <LeftBar />
                </div>
                <div className="col-12 col-sm-9 col-md-10 col-lg-10 col-xl-10 col-xxl-10 px-4">
                    <form className="row pt-4">
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Banner</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Theme color</label>
                                    <input type="color" className="form-control" style={{ width: "50px", height: "37px" }} />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <div className="row flex-nowrap">
                                        <div className="flex-grow-1 flex-shrink-1 w-100">
                                            <label className="form-label">Background image</label>
                                            <input type="file" className="form-control" />
                                        </div>
                                        <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                            <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Main content</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="MainContentAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="MainContentHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#MainContentBody-1" aria-expanded="false" aria-controls="MainContentBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="MainContentBody-1" className="accordion-collapse collapse" aria-labelledby="MainContentHead-1" data-bs-parent="#MainContentAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="MainContentHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#MainContentBody-2" aria-expanded="false" aria-controls="MainContentBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="MainContentBody-2" className="accordion-collapse collapse" aria-labelledby="MainContentHead-2" data-bs-parent="#MainContentAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Differentiators</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="DifferentiatorsAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="DifferentiatorsHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#DifferentiatorsBody-1" aria-expanded="false" aria-controls="DifferentiatorsBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="DifferentiatorsBody-1" className="accordion-collapse collapse" aria-labelledby="DifferentiatorsHead-1" data-bs-parent="#DifferentiatorsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Text</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="DifferentiatorsHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#DifferentiatorsBody-2" aria-expanded="false" aria-controls="DifferentiatorsBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="DifferentiatorsBody-2" className="accordion-collapse collapse" aria-labelledby="DifferentiatorsHead-2" data-bs-parent="#DifferentiatorsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Text</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Insights</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="InsightsAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="InsightsHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#InsightsBody-1" aria-expanded="false" aria-controls="InsightsBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="InsightsBody-1" className="accordion-collapse collapse" aria-labelledby="InsightsHead-1" data-bs-parent="#InsightsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Text</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Button type</label>
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="radio" name="Box-1-InsightRadio" id="Box-1-InsightRadio-1" value="Read more" />
                                                                <label className="form-check-label" htmlFor="Box-1-InsightRadio-1">Read more</label>
                                                            </div>
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="radio" name="Box-1-InsightRadio" id="Box-1-InsightRadio-2" value="Download" />
                                                                <label className="form-check-label" htmlFor="Box-1-InsightRadio-2">Download</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="InsightsHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#InsightsBody-2" aria-expanded="false" aria-controls="InsightsBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="InsightsBody-2" className="accordion-collapse collapse" aria-labelledby="InsightsHead-2" data-bs-parent="#InsightsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Text</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Button type</label>
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="radio" name="Box-2-InsightRadio" id="Box-2-InsightRadio-1" value="Read more" />
                                                                <label className="form-check-label" htmlFor="Box-2-InsightRadio-1">Read more</label>
                                                            </div>
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="radio" name="Box-2-InsightRadio" id="Box-2-InsightRadio-2" value="Download" />
                                                                <label className="form-check-label" htmlFor="Box-2-InsightRadio-2">Download</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-3 d-flex justify-content-end">
                            <button type="button" className="btn btn-success me-3">Publish</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default WebDevelopmentForm