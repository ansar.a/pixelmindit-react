import React from 'react'
import { NavLink } from 'react-router-dom';

function LeftBar() {
    return (
        <div className="row h-100">
            <div className="col-12 bg-dark min-vh-100">
                <nav class="navbar navbar-expand navbar-dark bg-dark">
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav flex-column w-100">
                            <li class="nav-item">
                                <NavLink to="/" className="nav-link" href="#">Dashboard</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/header-footer" className="nav-link" href="#">Header/Footer</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/home" className="nav-link" href="#">Home</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Services</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Solutions</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Industries</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Company</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Careers</NavLink>
                            </li>
                            <li class="nav-item">
                                <NavLink to="/a" className="nav-link" href="#">Contact Us</NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export default LeftBar