import React from 'react'
import CLogo from '../../../assets/PixelMindIT-logo-color.png'
import WLogo from '../../../assets/PixelMindIT-logo-white.png'

function TopBar() {
    return (
        <div className="row">
            <div className="col-12 bg-dark d-flex justify-content-between align-items-center py-2 border-bottom">
                <img src={WLogo} alt="Logo" width="200" />
                <div class="btn-group">
                    <button type="button" class="btn btn-light bg-white">
                        <img src={CLogo} alt="" width="30" height="30" className='rounded-circle me-2 border' />
                        <span>John Doe</span>
                    </button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="#">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default TopBar