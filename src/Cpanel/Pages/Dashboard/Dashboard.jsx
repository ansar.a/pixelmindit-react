import React from 'react'
import TopBar from '../Templates/TopBar'
import LeftBar from '../Templates/LeftBar'
import Logo from '../../../assets/PixelMindIT-logo-color.png'

function Dashboard() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <TopBar />
                </div>
                <div className="col-12 col-sm-3 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
                    <LeftBar />
                </div>
                <div className="col-12 col-sm-9 col-md-10 col-lg-10 col-xl-10 col-xxl-10 px-4">
                    <div className="row">
                        <div className="col-12 col-lg-6 mx-auto px-5 py-5 mt-5 bg-light border">
                            <img src={Logo} alt="Logo" width="200" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard