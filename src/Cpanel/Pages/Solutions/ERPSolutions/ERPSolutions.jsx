import React from 'react'
import TopBar from '../../Templates/TopBar'
import LeftBar from '../../Templates/LeftBar'
import CLogo from '../../../../assets/PixelMindIT-logo-color.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faClose } from '@fortawesome/free-solid-svg-icons'

function ERPSolutionsForm() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <TopBar />
                </div>
                <div className="col-12 col-sm-3 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
                    <LeftBar />
                </div>
                <div className="col-12 col-sm-9 col-md-10 col-lg-10 col-xl-10 col-xxl-10 px-4">
                    <form className="row pt-4">
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Banner</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <div className="row flex-nowrap">
                                        <div className="flex-grow-1 flex-shrink-1 w-100">
                                            <label className="form-label">Background image</label>
                                            <input type="file" className="form-control" />
                                        </div>
                                        <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                            <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Main Text</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="MainTextAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="MainTextHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#MainTextBody-1" aria-expanded="false" aria-controls="MainTextBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="MainTextBody-1" className="accordion-collapse collapse" aria-labelledby="MainTextHead-1" data-bs-parent="#MainTextAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Paragraph</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="MainTextHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#MainTextBody-2" aria-expanded="false" aria-controls="MainTextBody-2">Box 2</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="MainTextBody-2" className="accordion-collapse collapse" aria-labelledby="MainTextHead-2" data-bs-parent="#MainTextAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Paragraph</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Advantages</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="AdvantagesAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="AdvantagesHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#AdvantagesBody-1" aria-expanded="false" aria-controls="AdvantagesBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="AdvantagesBody-1" className="accordion-collapse collapse" aria-labelledby="AdvantagesHead-1" data-bs-parent="#AdvantagesAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="AdvantagesHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#AdvantagesBody-2" aria-expanded="false" aria-controls="AdvantagesBody-2">Box 2</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="AdvantagesBody-2" className="accordion-collapse collapse" aria-labelledby="AdvantagesHead-2" data-bs-parent="#AdvantagesAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-3 d-flex justify-content-end">
                            <button type="button" className="btn btn-success me-3">Publish</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ERPSolutionsForm