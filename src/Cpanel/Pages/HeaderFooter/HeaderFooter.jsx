import React from 'react'
import TopBar from '../Templates/TopBar'
import LeftBar from '../Templates/LeftBar'
import CLogo from '../../../assets/PixelMindIT-logo-color.png'
import WLogo from '../../../assets/PixelMindIT-logo-white.png'

function HeaderFooterForm() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <TopBar />
                </div>
                <div className="col-12 col-sm-3 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
                    <LeftBar />
                </div>
                <div className="col-12 col-sm-9 col-md-10 col-lg-10 col-xl-10 col-xxl-10 px-4">
                    <form className="row pt-4">
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Logos</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <div className="row flex-nowrap">
                                        <div className="flex-grow-1 flex-shrink-1 w-100">
                                            <label className="form-label">Logo in white background</label>
                                            <input type="file" className="form-control" />
                                        </div>
                                        <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                            <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <div className="row flex-nowrap">
                                        <div className="flex-grow-1 flex-shrink-1 w-100">
                                            <label className="form-label">Logo in black background</label>
                                            <input type="file" className="form-control" />
                                        </div>
                                        <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                            <div className='border rounded p-1 d-flex bg-dark' style={{ height: "70px" }}>
                                                <img src={WLogo} className="w-100 h-100 object-fit-contain" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Connection Links</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Twitter</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Facebook</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Linked In</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Copyright</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Footer Copyright text (Date already there at begining)</label>
                                    <div class="input-group">
                                        <span class="input-group-text">2023</span>
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-3 d-flex justify-content-end">
                            <button type="button" className="btn btn-success me-3">Publish</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HeaderFooterForm