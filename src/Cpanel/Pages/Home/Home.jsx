import React from 'react'
import TopBar from '../Templates/TopBar'
import LeftBar from '../Templates/LeftBar'
import CLogo from '../../../assets/PixelMindIT-logo-color.png'
import WLogo from '../../../assets/PixelMindIT-logo-white.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faClose } from '@fortawesome/free-solid-svg-icons'

function HomeForm() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <TopBar />
                </div>
                <div className="col-12 col-sm-3 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
                    <LeftBar />
                </div>
                <div className="col-12 col-sm-9 col-md-10 col-lg-10 col-xl-10 col-xxl-10 px-4">
                    <form className="row pt-4">
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Banner</h5>
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="BannerAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="BannerHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#BannerBody-1" aria-expanded="false" aria-controls="BannerBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="BannerBody-1" className="accordion-collapse collapse" aria-labelledby="BannerHead-1" data-bs-parent="#BannerAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-dark' style={{ height: "70px" }}>
                                                                        <img src={WLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="BannerHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#BannerBody-2" aria-expanded="false" aria-controls="BannerBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="BannerBody-2" className="accordion-collapse collapse" aria-labelledby="BannerHead-2" data-bs-parent="#BannerAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-dark' style={{ height: "70px" }}>
                                                                        <img src={WLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                </div>
                                <div className='col-12'>
                                    <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                </div>
                            </div>
                        </div>

                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>About us</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Description</label>
                                    <textarea rows="3" className="form-control" />
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Services (5)</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="ServiceAccordion">
                                        {/* fixed loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ServiceHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ServiceBody-1" aria-expanded="false" aria-controls="ServiceBody-1">Box 1</button>
                                            </h2>
                                            <div id="ServiceBody-1" className="accordion-collapse collapse" aria-labelledby="ServiceHead-1" data-bs-parent="#ServiceAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* fixed loop here over */}
                                        {/* fixed loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ServiceHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ServiceBody-2" aria-expanded="false" aria-controls="ServiceBody-2">Box 1</button>
                                            </h2>
                                            <div id="ServiceBody-2" className="accordion-collapse collapse" aria-labelledby="ServiceHead-2" data-bs-parent="#ServiceAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* fixed loop here 2 over */}
                                        {/* fixed loop here 3 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ServiceHead-3">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ServiceBody-3" aria-expanded="false" aria-controls="ServiceBody-3">Box 1</button>
                                            </h2>
                                            <div id="ServiceBody-3" className="accordion-collapse collapse" aria-labelledby="ServiceHead-3" data-bs-parent="#ServiceAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* fixed loop here 3 over */}
                                        {/* fixed loop here 4 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ServiceHead-4">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ServiceBody-4" aria-expanded="false" aria-controls="ServiceBody-4">Box 1</button>
                                            </h2>
                                            <div id="ServiceBody-4" className="accordion-collapse collapse" aria-labelledby="ServiceHead-4" data-bs-parent="#ServiceAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* fixed loop here 4 over */}
                                        {/* fixed loop here 5 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ServiceHead-5">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ServiceBody-5" aria-expanded="false" aria-controls="ServiceBody-5">Box 1</button>
                                            </h2>
                                            <div id="ServiceBody-5" className="accordion-collapse collapse" aria-labelledby="ServiceHead-5" data-bs-parent="#ServiceAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Background image</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Link</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* fixed loop here 5 over */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Contactable text</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Contact text</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Contact link</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Clients</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Description</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="ClientsAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ClientsHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ClientsBody-1" aria-expanded="false" aria-controls="ClientsBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="ClientsBody-1" className="accordion-collapse collapse" aria-labelledby="ClientsHead-1" data-bs-parent="#ClientsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="ClientsHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#ClientsBody-2" aria-expanded="false" aria-controls="ClientsBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="ClientsBody-2" className="accordion-collapse collapse" aria-labelledby="ClientsHead-2" data-bs-parent="#ClientsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                    <div className='col-12'>
                                        <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Solutions</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="SolutionsAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="SolutionsHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#SolutionsBody-1" aria-expanded="false" aria-controls="SolutionsBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="SolutionsBody-1" className="accordion-collapse collapse" aria-labelledby="SolutionsHead-1" data-bs-parent="#SolutionsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Sub heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="SolutionsHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#SolutionsBody-2" aria-expanded="false" aria-controls="SolutionsBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="SolutionsBody-2" className="accordion-collapse collapse" aria-labelledby="SolutionsHead-2" data-bs-parent="#SolutionsAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Sub heading</label>
                                                            <input type="text" className="form-control" />
                                                        </div>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <label className="form-label">Description</label>
                                                            <textarea rows="3" className="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                    <div className='col-12'>
                                        <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-12 border-bottom mb-3'>
                            <div className='row'>
                                <div className='col-12'>
                                    <h5 className='mt-2'>Partners</h5>
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Heading</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className="col-12 col-lg-6 mb-3">
                                    <label className="form-label">Description</label>
                                    <input type="text" className="form-control" />
                                </div>
                                <div className='col-12'>
                                    <div className="accordion mb-3" id="PartnersAccordion">
                                        {/* loop here */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="PartnersHead-1">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#PartnersBody-1" aria-expanded="false" aria-controls="PartnersBody-1">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="PartnersBody-1" className="accordion-collapse collapse" aria-labelledby="PartnersHead-1" data-bs-parent="#PartnersAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here over */}
                                        {/* loop here 2 */}
                                        <div className="accordion-item">
                                            <h2 className="accordion-header position-relative" id="PartnersHead-2">
                                                <button className="accordion-button collapsed fw-bold bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#PartnersBody-2" aria-expanded="false" aria-controls="PartnersBody-2">Box 1</button>
                                                <button type='button' className='btn btn-danger btn-sm accordion-closer'><FontAwesomeIcon icon={faClose} /></button>
                                            </h2>
                                            <div id="PartnersBody-2" className="accordion-collapse collapse" aria-labelledby="PartnersHead-2" data-bs-parent="#PartnersAccordion">
                                                <div className="accordion-body bg-light">
                                                    <div className='row'>
                                                        <div className="col-12 col-lg-6 mb-3">
                                                            <div className="row flex-nowrap">
                                                                <div className="flex-grow-1 flex-shrink-1 w-100">
                                                                    <label className="form-label">Logo</label>
                                                                    <input type="file" className="form-control" />
                                                                </div>
                                                                <div className="ps-0 flex-grow-0 flex-shrink-0" style={{ width: "82px" }}>
                                                                    <div className='border rounded p-1 d-flex bg-white' style={{ height: "70px" }}>
                                                                        <img src={CLogo} className="w-100 h-100 object-fit-contain" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* loop here 2 over */}
                                    </div>
                                    <div className='col-12'>
                                        <button type='button' className='btn btn-primary btn-sm mb-3'><FontAwesomeIcon icon={faPlus} /><span className='ms-2'>Add new</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-3 d-flex justify-content-end">
                            <button type="button" className="btn btn-success me-3">Publish</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HomeForm